#
# DO NOT DELETE THESE LINES UNTIL INSTRUCTED TO!
#
# Your AMI ID is:
#
#     ami-0fcfd45b96222a2ae
#
# Your subnet ID is:
#
#     subnet-01006e200d3151f9d
#
# Your VPC security group ID is:
#
#     sg-01989f120ea70a092
#
# Your Identity is:
#
#     terraform-training-chicken
#

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
  version    = ">= 2.27.0"
}

# resource "aws_instance" "web" {
#   ami           = var.ami
#   instance_type = "t2.micro"

#   subnet_id              = var.subnet_id
#   vpc_security_group_ids = var.vpc_security_group_ids


#   tags = {
#     "Identity" = var.identity
#     "Name"        = "Student"
#     "Environment" = "Training"
#   }
# }

module "server" {
  source  = "tfe.couchtocloud.com/FAA/chicken/aws"
  version = "0.0.1"

  ami                    = var.ami
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids
  identity               = var.identity
  key_name               = module.keypair.key_name
  private_key            = module.keypair.private_key_pem
}

module "keypair" {
  source  = "mitchellh/dynamic-keys/aws"
  version = "2.0.0"
  path    = "${path.root}/keys"
  name    = "${var.identity}-key-2"
}

output "public_ip" {
  value = module.server.public_ip
}

output "public_dns" {
  value = module.server.public_dns
}

# output "random" {
#   value = random_id.random.hex
# }

# output "write_state_random" {
#   value = data.terraform_remote_state.write_state.outputs.random
# }

