variable "access_key" {}
variable "secret_key" {}
variable "region" {}
variable "ami" {}
variable "subnet_id" {}
variable "identity" {}
variable "vpc_security_group_ids" {
  type = list
}



